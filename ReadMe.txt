//////////////////////ESSAY///////////////////////////////

 We want to check our trainers' list on mobile and be able to update it.
 For testing purposes, we are going to use REST API to get bunch of trainers extracted from our database.

 REST API endpoint: https://5fb52c64e473ab0016a179a0.mockapi.io/api/v1/employee/employee

 
 -§- TrainerListActivity -§-

 We decided to show the trainer array in a RecyclerView, and for each trainer show:
 - name
 - picture
 - email
 
 When the user selects a trainer from the list by tapping on the TrainerListSelectionRecyclerViewAdapter the TrainerDetailsActivity will open.
 
 -§- TrainerDetailsActivity -§-
 
 Inside the TrainerDetailsActivity the user can see the following info of the trainer:
 - picture
 - name
 - email
 - availability
 - favoriteFruit
 - skills
 

 
 
 UserInterface:
 Feel free to express your self with UI, decide where to put each element, only be sure to respect functionalities guidelines
 
 Mandatory:
 - The project must be in Kotlin code
 - The project must compile
 - Use suitable design pattern
 - Add unit tests and UI test cases
 - You have to submit your final version after 72h since you receive this essay
 - The UI must adapt to different phone sizes
 
 Important:
 - Write organized and reusable code
 - RecyclerView with trainers should scroll smoothly

 
 Bonus points:
 - User can search in employees list
 
 
 If you have any questions, feel free to contact me by e-mail (sara@lepaya.com), I will do my best to answer as quickly as possible.
 
