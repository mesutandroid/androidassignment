package com.example.androidassignment.trainer

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.androidassignment.data.model.Trainer
import com.google.common.truth.Truth.assertThat
import com.example.androidassignment.data.remote.Api
import com.example.androidassignment.data.remote.WebService
import com.example.androidassignment.data.repository.trainer.TrainersWebRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.doReturn
import org.mockito.Mockito.times
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class TrainersWebRepositoryTest {

    @Mock
    lateinit var webService: WebService

    @Mock
    lateinit var trainersWebRepository: TrainersWebRepository

    // Executes each task synchronously using Architecture Components.
    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @Test
    fun whenFetchTrainerGroupList_shouldReturnEmptyList() {
        runBlockingTest {
            doReturn(arrayListOf<Trainer>())
                .`when`(webService)
                .getTrainerList(Api.BASE_URL)

            val trainersWebRepository = TrainersWebRepository(webService)
            val trainerList = trainersWebRepository.getTrainerList()
            assertThat(trainerList.isEmpty())
        }
    }

    @Test
    fun whenFetchTrainerGroupList_shouldCallApi() {
        runBlockingTest {
            trainersWebRepository.getTrainerList()
            Mockito.verify(trainersWebRepository, times(1)).getTrainerList()
        }
    }

}