package com.example.androidassignment.ui.trainers

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.androidassignment.data.model.Result
import com.example.androidassignment.data.model.Trainer
import com.example.androidassignment.data.repository.trainer.TrainersRepository
import com.example.androidassignment.util.Event
import com.example.androidassignment.utils.TestCoroutineRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner


@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class TrainersViewModelTest {

    @Mock
    private lateinit var trainersRepository: TrainersRepository

    @Mock
    private lateinit var observer: Observer<Event<Result<List<Trainer>>>>

    @Captor
    private lateinit var captor: ArgumentCaptor<Event<Result<List<Trainer>>>>

    // Executes each task synchronously using Architecture Components.
    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Test
    fun givenMethodInvocation_whenFetchTrainerList_shouldReturnLoading() {
        testCoroutineRule.runBlockingTest {
            val viewModel = TrainersViewModel(trainersRepository)
            viewModel.trainers.observeForever(observer)
            viewModel.fetchTrainers()
            verify(trainersRepository).getTrainerList()
            verify(observer, times(2)).onChanged(captor.capture())

            captor.allValues[0].getContentIfNotHandled().let {
                val excepted = Event(Result.loading(data = null)).getContentIfNotHandled()
                val observedValue = it
                assertEquals(excepted?.status, observedValue?.status)
                assertEquals(excepted?.data, observedValue?.data)
            }

            viewModel.trainers.removeObserver(observer)
        }
    }

    @Test
    fun givenSuccess_whenFetchTrainerList_shouldReturnSuccess() {
        testCoroutineRule.runBlockingTest {
            doReturn(arrayListOf<Trainer>())
                .`when`(trainersRepository)
                .getTrainerList()

            val viewModel = TrainersViewModel(trainersRepository)
            viewModel.trainers.observeForever(observer)
            viewModel.fetchTrainers()
            verify(trainersRepository).getTrainerList()

            captor.run {
                verify(observer, times(2)).onChanged(capture())
                val excepted =
                    Event(Result.success(data = arrayListOf<Trainer>())).getContentIfNotHandled()
                val observedValue = value.getContentIfNotHandled()
                assertEquals(excepted?.status, observedValue?.status)
                assertEquals(excepted?.data, observedValue?.data)
            }

            viewModel.trainers.removeObserver(observer)
        }
    }

    @Test
    fun givenError_whenFetchTrainerList_shouldReturnError() {
        testCoroutineRule.runBlockingTest {
            val errorMessage = "Error Message"
            doThrow(RuntimeException(errorMessage))
                .`when`(trainersRepository)
                .getTrainerList()

            val viewModel = TrainersViewModel(trainersRepository)
            viewModel.trainers.observeForever(observer)
            viewModel.fetchTrainers()
            verify(trainersRepository).getTrainerList()

            captor.run {
                verify(observer, times(2)).onChanged(capture())
                val excepted = Event(Result.error(errorMessage, null)).getContentIfNotHandled()
                val observedValue = value.getContentIfNotHandled()
                assertEquals(excepted?.status, observedValue?.status)
                assertEquals(excepted?.data, observedValue?.data)
            }

            viewModel.trainers.removeObserver(observer)
        }
    }

}