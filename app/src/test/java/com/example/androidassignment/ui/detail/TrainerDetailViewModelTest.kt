package com.example.androidassignment.ui.detail

import com.google.common.truth.Truth.assertThat
import com.example.androidassignment.data.model.Trainer
import org.junit.Test

class TrainerDetailViewModelTest {

    @Test
    fun givenTrainer_whenStart_shouldReturnTrainer() {
        val trainer = Trainer()
        val vm = TrainerDetailViewModel()
        vm.start(trainer)
        assertThat(vm.trainer == trainer)
    }

}