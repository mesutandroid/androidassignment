package com.example.androidassignment.di

import androidx.lifecycle.ViewModel
import com.example.androidassignment.ui.trainers.TrainersFragment
import com.example.androidassignment.ui.trainers.TrainersViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class TrainersModule {

    @ContributesAndroidInjector(
        modules = [
            ViewModelBuilder::class
        ]
    )
    internal abstract fun trainersFragment(): TrainersFragment

    @Binds
    @IntoMap
    @ViewModelKey(TrainersViewModel::class)
    abstract fun bindViewModel(vm: TrainersViewModel): ViewModel

}