package com.example.androidassignment.di

import com.example.androidassignment.data.remote.Api
import com.example.androidassignment.data.remote.WebService
import com.example.androidassignment.data.repository.trainer.TrainersRepository
import com.example.androidassignment.data.repository.trainer.TrainersWebRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module(includes = [ApplicationModuleBinds::class])
object ApplicationModule {

    @Provides
    @Singleton
    fun provideWebService(): WebService {
        return Api.create()
    }

}

@Module
abstract class ApplicationModuleBinds {

    @Singleton
    @Binds
    abstract fun bindRepository(repo: TrainersWebRepository): TrainersRepository
}
