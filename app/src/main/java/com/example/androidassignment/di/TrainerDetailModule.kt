package com.example.androidassignment.di

import androidx.lifecycle.ViewModel
import com.example.androidassignment.ui.detail.TrainerDetailFragment
import com.example.androidassignment.ui.detail.TrainerDetailViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class TrainerDetailModule {

    @ContributesAndroidInjector(
        modules = [
            ViewModelBuilder::class
        ]
    )
    internal abstract fun trainerDetailFragment(): TrainerDetailFragment

    @Binds
    @IntoMap
    @ViewModelKey(TrainerDetailViewModel::class)
    abstract fun bindViewModel(vm: TrainerDetailViewModel): ViewModel


}