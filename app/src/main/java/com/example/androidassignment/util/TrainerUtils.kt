package com.example.androidassignment.util

import com.example.androidassignment.data.model.Name

class TrainerUtils {

    companion object {

        @JvmStatic
        fun getFullName(name: Name): String {
            return "${name.first} ${name.last}"
        }

        @JvmStatic
        fun isAvailable(isAvailable: Boolean): String {
            return if (isAvailable) "Available" else "Unavailable"
        }

    }
}