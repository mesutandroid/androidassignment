package com.example.androidassignment.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.example.androidassignment.data.remote.Api
import com.example.androidassignment.databinding.TrainerDetailBinding
import com.example.androidassignment.util.loadUrl
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.trainer_detail.*
import javax.inject.Inject

class TrainerDetailFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by viewModels<TrainerDetailViewModel> { viewModelFactory }

    private val args: TrainerDetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        viewModel.start(args.trainer)
        val binding = TrainerDetailBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = this@TrainerDetailFragment
            viewmodel = viewModel
        }

        return binding.root
    }

}
