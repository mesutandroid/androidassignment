package com.example.androidassignment.ui.detail

import androidx.lifecycle.ViewModel
import com.example.androidassignment.data.model.Trainer
import javax.inject.Inject

class TrainerDetailViewModel @Inject constructor() : ViewModel() {

    var trainer: Trainer? = null

    fun start(trainer: Trainer) {
        this.trainer = trainer
    }

}