package com.example.androidassignment.ui.trainers

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.androidassignment.data.model.Trainer
import com.example.androidassignment.data.model.Result
import com.example.androidassignment.data.repository.trainer.TrainersRepository
import com.example.androidassignment.util.Event
import kotlinx.coroutines.launch
import javax.inject.Inject

class TrainersViewModel @Inject constructor(private val repository: TrainersRepository) :
    ViewModel() {

    private val _openTrainerEvent = MutableLiveData<Event<Trainer>>()
    val openTrainerEvent: LiveData<Event<Trainer>> = _openTrainerEvent

    private val _trainers = MutableLiveData<Event<Result<ArrayList<Trainer>>>>()
    val trainers: LiveData<Event<Result<ArrayList<Trainer>>>> = _trainers

    fun fetchTrainers() = viewModelScope.launch {
        _trainers.value = Event(Result.loading(data = null))
        try {
            val trainerList = repository.getTrainerList()
            _trainers.value = Event(Result.success(data = trainerList))
        } catch (exception: Exception) {
            val error = Event(
                Result.error(
                    data = null,
                    message = exception.message ?: "Error occured"
                )
            )
            _trainers.value = (error)
        }
    }

    fun openTrainerDetail(Trainer: Trainer) {
        _openTrainerEvent.value = Event(Trainer)
    }

}