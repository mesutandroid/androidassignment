package com.example.androidassignment.ui.trainers


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.androidassignment.data.model.Trainer
import com.example.androidassignment.databinding.TrainerItemBinding

class TrainersRecyclerViewAdapter(
    private val viewModel: TrainersViewModel
) : RecyclerView.Adapter<TrainersRecyclerViewAdapter.ViewHolder>() {

    private var trainerList: List<Trainer> = emptyList()

    fun setItems(Trainers: List<Trainer>) {
        this.trainerList = Trainers
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(viewModel, getItem(position))
    }

    private fun getItem(position: Int): Trainer {
        return trainerList[position]
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int {
        return trainerList.size
    }

    class ViewHolder private constructor(private val binding: TrainerItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(viewModel: TrainersViewModel, item: Trainer) {
            binding.viewmodel = viewModel
            binding.trainer = item
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = TrainerItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding)
            }
        }
    }


}
