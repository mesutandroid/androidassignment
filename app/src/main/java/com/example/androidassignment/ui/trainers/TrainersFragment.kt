package com.example.androidassignment.ui.trainers

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.androidassignment.data.model.Trainer
import com.example.androidassignment.data.model.Result.Status
import com.example.androidassignment.databinding.TrainersFragmentBinding
import com.example.androidassignment.util.EventObserver
import com.example.androidassignment.util.test.EspressoIdlingResource
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.trainers_fragment.*
import timber.log.Timber
import javax.inject.Inject


class TrainersFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by viewModels<TrainersViewModel> { viewModelFactory }

    private lateinit var binding: TrainersFragmentBinding
    private lateinit var adapter: TrainersRecyclerViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = TrainersFragmentBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = this@TrainersFragment
            viewModel = this@TrainersFragment.viewModel
        }

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setupRecyclerAdapter()
        setupNavigation()
        setupObservers()
        viewModel.fetchTrainers()
    }

    private fun setupObservers() {
        viewModel.trainers.observe(viewLifecycleOwner, {
            it?.getContentIfNotHandled().let { resource ->
                when (resource?.status) {
                    Status.SUCCESS -> {
                        progressBar.visibility = View.GONE
                        recyclerView.visibility = View.VISIBLE
                        resource.data?.let { trainerList ->
                            updateAdapterItems(trainerList)
                        }
                        EspressoIdlingResource.decrement()
                    }
                    Status.ERROR -> {
                        recyclerView.visibility = View.VISIBLE
                        progressBar.visibility = View.GONE
                        Toast.makeText(context, resource.message, Toast.LENGTH_LONG).show()
                    }
                    Status.LOADING -> {
                        progressBar.visibility = View.VISIBLE
                        recyclerView.visibility = View.GONE
                        EspressoIdlingResource.increment()
                    }
                }
            }
        })
    }

    private fun updateAdapterItems(trainerList: ArrayList<Trainer>) {
        trainerList.let { adapter.setItems(it) }
    }

    private fun setupRecyclerAdapter() {
        val viewModel = binding.viewModel
        if (viewModel != null) {
            adapter = TrainersRecyclerViewAdapter(viewModel)
            binding.recyclerView.adapter = adapter
            binding.recyclerView.itemAnimator = null
        } else {
            Timber.w("ViewModel not initialized when attempting to set up adapter.")
        }
    }

    private fun setupNavigation() {
        viewModel.openTrainerEvent.observe(viewLifecycleOwner, EventObserver {
            openTrainerDetail(it)
        })
    }

    private fun openTrainerDetail(trainer: Trainer) {
        val action =
            TrainersFragmentDirections.actionTrainersFragmentToTrainerDetailFragment(trainer)
        findNavController().navigate(action)
    }

}
