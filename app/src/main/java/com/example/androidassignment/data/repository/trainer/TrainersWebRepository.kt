package com.example.androidassignment.data.repository.trainer

import com.example.androidassignment.data.model.Trainer
import com.example.androidassignment.data.remote.Api
import com.example.androidassignment.data.remote.WebService
import javax.inject.Inject

class TrainersWebRepository @Inject constructor(private val webservice: WebService) :
    TrainersRepository {

    override suspend fun getTrainerList(): ArrayList<Trainer> {
        return webservice.getTrainerList(Api.BASE_URL)
    }

}