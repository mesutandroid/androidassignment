package com.example.androidassignment.data.remote

import com.example.androidassignment.data.model.Trainer
import retrofit2.http.GET
import retrofit2.http.Url

interface WebService {

    @GET("")
    suspend fun getTrainerList(@Url url: String): ArrayList<Trainer>

}