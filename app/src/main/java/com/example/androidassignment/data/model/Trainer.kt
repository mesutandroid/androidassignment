package com.example.androidassignment.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Trainer(
    val createdAt: String? = null,
    val name: Name? = null,
    val avatar: String? = null,
    val index: Int? = null,
    val guid: String? = null,
    val isAvailable: Boolean? = null,
    val picture: String? = null,
    val email: String? = null,
    val about: String? = null,
    val bornDate: String? = null,
    val tags: List<String?>? = null,
    val favoriteFruit: String? = null
) : Parcelable

@Parcelize
data class Name(
    val first: String?,
    val last: String?
) : Parcelable


