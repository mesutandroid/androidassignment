package com.example.androidassignment.data.repository.trainer

import com.example.androidassignment.data.model.Trainer

interface TrainersRepository {

    suspend fun getTrainerList(): ArrayList<Trainer>

}
