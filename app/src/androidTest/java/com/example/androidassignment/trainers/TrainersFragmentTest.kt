package com.example.androidassignment.trainers


import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.androidassignment.R
import com.example.androidassignment.data.FakeTrainerRepository
import com.example.androidassignment.ui.MainActivity
import com.example.androidassignment.ui.trainers.TrainersRecyclerViewAdapter
import com.example.androidassignment.util.TrainerUtils
import com.example.androidassignment.util.test.EspressoIdlingResource
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class TrainersFragmentTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityScenarioRule(MainActivity::class.java)

    private val LIST_ITEM_IN_TEST = 1
    private val Trainer_IN_TEST = FakeTrainerRepository.trainers[LIST_ITEM_IN_TEST]

    @Before
    fun registerIdlingResource() {
        IdlingRegistry.getInstance().register(EspressoIdlingResource.countingIdlingResource)
    }

    @Test
    fun test_isListFragmentVisible_onAppLaunch() {
        onView(withId(R.id.recyclerView)).check(matches(isDisplayed()))
    }

    @Test
    fun test_selectListItem_isDetailFragmentVisible() {
        // Click list item #LIST_ITEM_IN_TEST
        onView(withId(R.id.recyclerView))
            .perform(
                actionOnItemAtPosition<TrainersRecyclerViewAdapter.ViewHolder>
                    (LIST_ITEM_IN_TEST, click())
            )

        // Confirm nav to DetailFragment and display title
        onView(withId(R.id.tvTrainerDetailName)).check(matches(withText(TrainerUtils.getFullName(Trainer_IN_TEST.name!!))))
    }

    @Test
    fun test_backNavigation_toTrainersFragment() {
        // Click list item #LIST_ITEM_IN_TEST
        onView(withId(R.id.recyclerView))
            .perform(
                actionOnItemAtPosition<TrainersRecyclerViewAdapter.ViewHolder>(
                    LIST_ITEM_IN_TEST,
                    click()
                )
            )

        // Confirm nav to DetailFragment and display title
        onView(withId(R.id.tvTrainerDetailName)).check(matches(withText(TrainerUtils.getFullName(Trainer_IN_TEST.name!!))))

        pressBack()

        // Confirm TrainersFragment in view
        onView(withId(R.id.recyclerView)).check(matches(isDisplayed()))
    }

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.countingIdlingResource)
    }

}
