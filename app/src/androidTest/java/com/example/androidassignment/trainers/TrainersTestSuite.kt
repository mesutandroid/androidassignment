package com.example.androidassignment.trainers

import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite::class)
@Suite.SuiteClasses(
    TrainersFragmentTest::class,
    TrainerDetailFragmentTest::class
)
class TrainersTestSuite
