package com.example.androidassignment.trainers


import android.os.Bundle
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.androidassignment.R
import com.example.androidassignment.data.FakeTrainerRepository
import com.example.androidassignment.ui.detail.TrainerDetailFragment
import com.example.androidassignment.util.TrainerUtils
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class TrainerDetailFragmentTest {

    @Test
    fun test_isTrainerDataVisible() {
        // SETUP
        val bundle = Bundle()
        val trainer = FakeTrainerRepository.trainer

        bundle.putParcelable("trainer", trainer)

        launchFragmentInContainer<TrainerDetailFragment>(
            fragmentArgs = bundle
        )

        // VERIFY
        onView(withId(R.id.tvTrainerDetailName))
            .check(matches(withText(TrainerUtils.getFullName(trainer.name!!))))

        onView(withId(R.id.tvTrainerDetailEmail))
            .check(matches(withText(trainer.email)))

        onView(withId(R.id.tvTrainerDetailAvailability))
            .check(matches(withText(TrainerUtils.isAvailable(trainer.isAvailable!!))))

        onView(withId(R.id.tvTrainerDetailFavoriteFruit))
            .check(matches(withText(trainer.favoriteFruit)))
    }

}
