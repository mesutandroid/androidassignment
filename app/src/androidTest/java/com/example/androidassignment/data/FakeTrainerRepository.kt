package com.example.androidassignment.data

import androidx.test.platform.app.InstrumentationRegistry
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.example.androidassignment.data.model.Trainer
import java.io.IOException


object FakeTrainerRepository {

    val trainers: ArrayList<Trainer> = Gson().fromJson(
        "trainersResponse.json".getJsonDataFromAsset(),
        object : TypeToken<ArrayList<Trainer>>() {}.type)

    val trainer = trainers[0]

    private fun String.getJsonDataFromAsset(): String? {
        val jsonString: String
        try {
            jsonString = InstrumentationRegistry.getInstrumentation().context
                .assets.open(this).bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }

}